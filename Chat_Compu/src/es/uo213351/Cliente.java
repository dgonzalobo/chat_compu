package es.uo213351;

import java.io.*;

public class  Cliente  {
	private Respuesta [] iBuffer;
	private Comando [] oBuffer;
	private String nick;
	private String sala;
	private int indexIbuffer;
	private int indexObuffer;

	public Cliente(String name){ //Constructor
		nick=name;
		indexIbuffer=0;
		indexObuffer=0;
		iBuffer=new Respuesta[10];
		oBuffer=new Comando[10];
		sala="pruebas";
	}
	public static void main (String []args){ //main
		//String name=args[1];
		String name="Lobo";
		Cliente client = new Cliente(name);
		client.ejecutar();
	}
	public void ejecutar(){
		Print print=new Print(iBuffer,indexIbuffer); //Hilo salida por pantalla
		NetworkOut salida=new NetworkOut(oBuffer, indexObuffer); //Hilo salida a red
		DataInput leerTeclado=new DataInput(oBuffer, nick, sala, indexObuffer); //Hilo lectura por teclado
		NetworkInput entrada=new NetworkInput(iBuffer,indexIbuffer); //Hilo entrada de red

		try{
			entrada.start();
			
			print.start();
			print.join();
			
			leerTeclado.start();
			
			salida.start();
			salida.join();
			
		}
		catch(Exception e){
			System.err.println("Error en el arranque de los hilos. "+e);
		}
	}
}

/**
 *Hilo de entrada de datos
 */
class DataInput extends Thread{ 
	private Comando [] oBuffer=new Comando[10];
	private String nick; 
	private String sala;
	private String mensaje;
	private int indexObuffer;

	public DataInput(Comando [] outbuffer, String name, String room, int inObuffer){//Constructor 
		try{
			System.arraycopy(outbuffer,0,oBuffer,0,outbuffer.length);
		}
		catch(Exception e){
			System.err.println("Error en la copia del buffer, en DataInput "+e);
		}
		nick = name;
		sala = room;
		indexObuffer=inObuffer;
	}
	public void run(){
		System.out.println("Estamos en el run del DataInput.");
		readKeyboard();
	}
	public void readKeyboard(){
		try{
			BufferedReader rKeyb = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Escriba el mensaje: ");
			mensaje = rKeyb.readLine();
			System.out.println("El mensaje ha sido leido.");
			putInBuffer();
		}
		catch(IOException e){
			System.err.println("Fallo en la lectura por teclado"+e);
		}
	}
	public void putInBuffer(){
		Comando comando = new Comando(mensaje, nick, sala);
		try{
			oBuffer[indexObuffer]=comando;
			System.out.println("El mensaje ha sido introducido en el oBuffer.");
			if(indexObuffer<10){
				indexObuffer++;
			}
			else{
				indexObuffer=0;
			}
		}
		catch(Exception e){
			System.err.println("Error al insertar el comando en el buffer salida. "+e);
		}
	}
	public int getIndexBuffer(){//getter
		return indexObuffer;
	}
}

/**
 * Hilo de salida por red
 */
class NetworkOut extends Thread{ 
	private Comando [] oBuffer=new Comando[10];
	private Comando lastItem;
	private String msgSending;
	private int indexObuffer;
	private int lastIndObuffer=0;

	public NetworkOut(Comando [] outbuffer, int inObuffer){//Constructor
		try{
			System.arraycopy(outbuffer,0,oBuffer,0,outbuffer.length);
		}
		catch(Exception e){
			System.err.println("Error en la copia del buffer, en NetworkOut. "+e);
		}
		indexObuffer=inObuffer;
	}
	public void run(){
		System.out.println("Estamos en el run de NetworkOut");
		readFromBuffer();
	}
	public void readFromBuffer(){
		if(indexObuffer!=lastIndObuffer && indexObuffer>0){
			Comando aux=oBuffer[oBuffer.length];
			System.out.println("NetworkOut ha leido del oBuffer");
			lastItem=aux;
			sendComand();
			lastIndObuffer=indexObuffer;
		}
	}
	public void sendComand(){
		Network server=new Network();
		msgSending=lastItem.getTipo()+";"+lastItem.getNick()+";"+lastItem.getSala()+";"+lastItem.getMensaje();
		try{
			server.send(msgSending);
			System.out.println("El comando ha sido enviado al server.");
		}
		catch(Exception e){
			System.err.println("Error en el envio del comando al server. "+e);
		}
	}
}

/**
 * Hilo de entrada por red
 */
class NetworkInput extends Thread{
	private Respuesta[] iBuffer=new Respuesta[10];
	private String mensaje;
	private int indexIbuffer;
	private String[] auxStore=new String[4];

	public NetworkInput(Respuesta [] inbuffer, int indIbuffer){//Constructor
		try{
			System.arraycopy(inbuffer,0,iBuffer,0,inbuffer.length);
		}
		catch(Exception e){
			System.err.println("Error en la copia del buffer, en NetworkInput. "+e);
		}
		indexIbuffer=indIbuffer;
	}
	public void run(){
		readFromServer();
	}
	public void readFromServer(){
		Network server = new Network();
		try{
			mensaje=server.recv();
			System.out.println("Mensaje recibido del server: "+mensaje);
			putInBuffer();
		}
		catch(Exception e){
			System.err.println("Error en la receptcion de datos del servidor. "+e);
		}
	}
	public void putInBuffer(){
		try{
			separar();
			Respuesta answer=new Respuesta(auxStore[0],auxStore[1],auxStore[2],auxStore[3]);
			iBuffer[indexIbuffer]=answer;
			System.out.println("Se ha introducido el mensaje en el iBuffer, index="+indexIbuffer);
			if(indexIbuffer<10){
				indexIbuffer=indexIbuffer+1;
			}
			else{
				indexIbuffer=0;
			}
		}
		catch(Exception e2){
			System.err.println("Error en la introduccion del objeto en el buffer entrada. "+e2);

		}
	}
	public void separar(){
		try{
			auxStore=mensaje.split(";");
		}
		catch(Exception e){
			System.err.println("Error en la separacion del mensaje recibido. "+e);
		}
	}
	public int getIndexBuffer(){//getter
		return indexIbuffer;
	}
}

/**
 *Hilo de salida por pantalla
 */
class Print extends Thread{

	private Respuesta [] iBuffer=new Respuesta[10];
	private Respuesta newItem;
	private String screen;
	private int indexIbuffer;
	private int lastIndIbuffer;

	public Print(Respuesta [] inbuffer,int indIbuffer){//Constructor
		try{
		System.arraycopy(inbuffer,0,iBuffer,0,inbuffer.length);
		}
		catch(Exception e){
			System.err.println("Error en la copia del buffer, en print. "+e);
		}
		indexIbuffer=indIbuffer;
	}
	public void run(){
		System.out.println("Estamos en el Run de Print.");
		readFromBuffer();
	}
	public void readFromBuffer(){
		System.out.println("Estamos en el readFromBuffer del hilo Print");
		if(indexIbuffer>0){
			Respuesta aux=iBuffer[indexIbuffer];
			System.out.println("Se ha sacado la Respuesta del iBuffer.");
			lastIndIbuffer=indexIbuffer;
			newItem=aux;
			convertMessage();
			System.out.println("Se ha convertido el mensaje.");
			showInScreen();
		}
	}
	public void convertMessage(){
		if(newItem.getTipo().equals("/MSG")){
			screen=newItem.getSala()+"|"+newItem.getNick()+" "+newItem.getMensaje();
		}
		else{
			//A partir de aqu� se creer� una estructura de if, que cambiar� el mensaje en funcion
			//del tipo de respuesta recibida. 
		}
	}
	public void showInScreen(){
		System.out.println(screen);
	}
}


/**
 * Classe comando
 */
class Comando{
	private String mensaje;
	private String nick;
	private String tipo;
	private String sala;

	public Comando(String aux, String name, String room){//Constructor
		mensaje=aux;
		nick=name;
		sala=room;
	}
	public boolean msgOrComand(){
		if(mensaje.startsWith("/")){
			return true;
		}
		else{
			return false;
		}
	}
	public void identificar(){
		if(msgOrComand()){		
			tipo=mensaje; 
		}
		else{
			tipo= "/MSG";
		}
	}
	public String getMensaje(){//getter
		return mensaje;
	}
	public String getNick(){//getter
		return nick;
	}
	public String getTipo(){//getter
		return tipo;
	}
	public String getSala(){//getter
		return sala;
	}

}

/**
 * Clase respuesta
 */
class Respuesta{
	private String mensaje;
	private String tipo;
	private String sala;
	private String nick;

	public Respuesta(String type, String name, String room, String msg){//constructor 
		tipo=type;
		sala=room;
		nick=name;
		mensaje=msg;
	}
	public String getMensaje(){//getter
		return mensaje;
	}
	public String getNick(){//getter
		return nick;
	}
	public String getTipo(){//getter
		return tipo;
	}
	public String getSala(){//getter
		return sala;
	}
}


/*
 * NOTA: 
 * Se plantea una variable indexBuffer para regular los indices de los buffers, el problema actual es como
 * hacer que esta variable pase un hilo a otro. 
 * Soluci�n provisional un getter al hilo.  
 */
